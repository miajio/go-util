package read

// sliceInterface 切片interface
type sliceInterfece interface {
	InterfaceToString(slice []interface{}) []string  // InterfaceToString Slice Interface To string
	InterfaceToInt(vals []interface{}) []int         // InterfaceToInt Slice Interface To int
	InterfaceToInt8(vals []interface{}) []int8       // InterfaceToInt8 Slice Interface To int8
	InterfaceToInt16(vals []interface{}) []int16     // InterfaceToInt16 Slice Interface To int16
	InterfaceToInt32(vals []interface{}) []int32     // InterfaceToInt32 Slice Interface To int32
	InterfaceToInt64(vals []interface{}) []int64     // InterfaceToInt64 Slice Interface To int64
	InterfaceToUint(vals []interface{}) []uint       // InterfaceToUint Slice Interface To uint
	InterfaceToUint8(vals []interface{}) []uint8     // InterfaceToUint8 Slice Interface To uint8
	InterfaceToUint16(vals []interface{}) []uint16   // InterfaceToUint16 Slice Interface To uint16
	InterfaceToUint32(vals []interface{}) []uint32   // InterfaceToUint32 Slice Interface To uint32
	InterfaceToUint64(vals []interface{}) []uint64   // InterfaceToUint64 Slice Interface To uint64
	InterfaceToByte(vals []interface{}) []byte       // InterfaceToByte Slice Interface To byte
	InterfaceToFloat32(vals []interface{}) []float32 // InterfaceToFloat32 Slice Interface To float32
	InterfaceToFloat64(vals []interface{}) []float64 // InterfaceToFloat64 Slice Interface To float64
}

// slice 切片结构体
type slice struct{}

var Slice sliceInterfece = (*slice)(nil)

// InterfaceToString Slice Interface To string
func (*slice) InterfaceToString(vals []interface{}) []string {
	result := make([]string, len(vals))
	for i := range vals {
		result[i] = vals[i].(string)
	}
	return result
}

// InterfaceToInt Slice Interface To int
func (*slice) InterfaceToInt(vals []interface{}) []int {
	result := make([]int, len(vals))
	for i := range vals {
		result[i] = vals[i].(int)
	}
	return result
}

// InterfaceToInt8 Slice Interface To int8
func (*slice) InterfaceToInt8(vals []interface{}) []int8 {
	result := make([]int8, len(vals))
	for i := range vals {
		result[i] = vals[i].(int8)
	}
	return result
}

// InterfaceToInt16 Slice Interface To int16
func (*slice) InterfaceToInt16(vals []interface{}) []int16 {
	result := make([]int16, len(vals))
	for i := range vals {
		result[i] = vals[i].(int16)
	}
	return result
}

// InterfaceToInt32 Slice Interface To int32
func (*slice) InterfaceToInt32(vals []interface{}) []int32 {
	result := make([]int32, len(vals))
	for i := range vals {
		result[i] = vals[i].(int32)
	}
	return result
}

// InterfaceToInt64 Slice Interface To int64
func (*slice) InterfaceToInt64(vals []interface{}) []int64 {
	result := make([]int64, len(vals))
	for i := range vals {
		result[i] = vals[i].(int64)
	}
	return result
}

// InterfaceToUint Slice Interface To uint
func (*slice) InterfaceToUint(vals []interface{}) []uint {
	result := make([]uint, len(vals))
	for i := range vals {
		result[i] = vals[i].(uint)
	}
	return result
}

// InterfaceToUint8 Slice Interface To uint8
func (*slice) InterfaceToUint8(vals []interface{}) []uint8 {
	result := make([]uint8, len(vals))
	for i := range vals {
		result[i] = vals[i].(uint8)
	}
	return result
}

// InterfaceToUint16 Slice Interface To uint16
func (*slice) InterfaceToUint16(vals []interface{}) []uint16 {
	result := make([]uint16, len(vals))
	for i := range vals {
		result[i] = vals[i].(uint16)
	}
	return result
}

// InterfaceToUint32 Slice Interface To uint32
func (*slice) InterfaceToUint32(vals []interface{}) []uint32 {
	result := make([]uint32, len(vals))
	for i := range vals {
		result[i] = vals[i].(uint32)
	}
	return result
}

// InterfaceToUint64 Slice Interface To uint64
func (*slice) InterfaceToUint64(vals []interface{}) []uint64 {
	result := make([]uint64, len(vals))
	for i := range vals {
		result[i] = vals[i].(uint64)
	}
	return result
}

// InterfaceToByte Slice Interface To byte
func (*slice) InterfaceToByte(vals []interface{}) []byte {
	result := make([]byte, len(vals))
	for i := range vals {
		result[i] = vals[i].(byte)
	}
	return result
}

// InterfaceToFloat32 Slice Interface To float32
func (*slice) InterfaceToFloat32(vals []interface{}) []float32 {
	result := make([]float32, len(vals))
	for i := range vals {
		result[i] = vals[i].(float32)
	}
	return result
}

// InterfaceToFloat64 Slice Interface To float64
func (*slice) InterfaceToFloat64(vals []interface{}) []float64 {
	result := make([]float64, len(vals))
	for i := range vals {
		result[i] = vals[i].(float64)
	}
	return result
}
