package log_test

import (
	"testing"

	"gitee.com/miajio/go-util/lib/log"
	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
)

func TestLog(t *testing.T) {
	l := &log.LoggerParam{
		Path:       "./",
		MaxSize:    512,
		MaxBackups: 16,
		MaxAge:     7,
		Compress:   true,
	}
	c := l.New(map[string]func(level zapcore.Level) bool{
		"info.log": func(level zapcore.Level) bool {
			return level < zapcore.WarnLevel && level >= zap.InfoLevel
		},
	})
	c.Info("success")

}
