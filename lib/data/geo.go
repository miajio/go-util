package data

import "math"

const (
	BaiduEarthRadius  float64 = 6378.137 // 百度地图使用的地球半径
	GoogleEarthRadius float64 = 6371.393 // Google地图使用的地图半径
)

// BaiduGeoDistance 百度地图距离计算 返回距离单位为m
// lat1 纬度1
// lng1 经度1
// lat2 纬度2
// lng2 经度2
func BaiduGeoDistance(lat1, lng1, lat2, lng2 float64) float64 {
	return GeoDistance(lat1, lng1, lat2, lng2, BaiduEarthRadius)
}

// GoogelGeoDistance Google地图距离计算 返回距离单位为m
// lat1 纬度1
// lng1 经度1
// lat2 纬度2
// lng2 经度2
func GoogelGeoDistance(lat1, lng1, lat2, lng2 float64) float64 {
	return GeoDistance(lat1, lng1, lat2, lng2, GoogleEarthRadius)
}

// GeoDistance 地图距离计算 返回距离单位为m
// lat1			纬度1
// lng1			经度1
// lat2			纬度2
// lng2			经度2
// earthRadius	地球半径
func GeoDistance(lat1, lng1, lat2, lng2 float64, earthRadius float64) float64 {

	redLat1 := lat1 * math.Pi / 180
	redLat2 := lat2 * math.Pi / 180

	redLng1 := lng1 * math.Pi / 180
	redLng2 := lng2 * math.Pi / 180
	return math.Acos(math.Sin(redLat1)*math.Sin(redLat2)+math.Cos(redLat1)*math.Cos(redLat2)*math.Cos(redLng2-redLng1)) * earthRadius * 1000
}
