package data

import (
	"crypto/hmac"
	"crypto/md5"
	"crypto/sha256"
	"encoding/hex"
	"fmt"
)

// encryptionInterface 加密接口
type encryptionInterface interface {
	MD5(val string) string // MD5加密
}

// encryption 加密结构体
type encryption struct{}

// Encryption 加密操作对象
var Encryption encryptionInterface = (*encryption)(nil)

// MD5 加密
func (encryption *encryption) MD5(val string) string {
	return fmt.Sprintf("%x", md5.Sum([]byte(val)))
}

// Sha256 加密
func (encryption *encryption) Sha256(val, key string) string {
	m := hmac.New(sha256.New, []byte(key))
	m.Write([]byte(val))
	return hex.EncodeToString(m.Sum(nil))
}
