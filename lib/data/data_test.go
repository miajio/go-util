package data_test

import (
	"fmt"
	"testing"

	"gitee.com/miajio/go-util/lib/data"
)

func TestDistinct(t *testing.T) {
	fmt.Println(data.Distinct.Int(1, 2, 5, 1, 3))
}

func TestFilter(t *testing.T) {
	fmt.Println(data.Fileter.Fileter([]int{1, 2, 4, 5, 1, 2}, func(val interface{}) bool {
		return val != 2
	}))
}
