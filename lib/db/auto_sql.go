package db

// 自动生成sql语句接口
type autoSQLInterface interface {
	Create(primaryKeyColumnTag, columnTag string, primaryKeyAutoType PrimaryKeyAutoType, val interface{}) *AutoSQLEngine // 创建sql模型
}

// 自动生成sql语句结构体
type autoSQL struct{}

var AutoSQL autoSQLInterface = (*autoSQL)(nil)

// Create 				创建sql模型
// primaryKeyColumnTag 	id字段标签
// columnTag 			字段标签
// primaryKeyAutoType	主键自动生成方式 None 不使用
// val 					主表结构体
func (autoSQL *autoSQL) Create(primaryKeyColumnTag, columnTag string, primaryKeyAutoType PrimaryKeyAutoType, val interface{}) *AutoSQLEngine {
	if val == nil {
		panic("autoSQL.Create val is nil")
	}
	return &AutoSQLEngine{
		PrimaryKeyColumnTag: primaryKeyColumnTag,
		ColumnTag:           columnTag,
		Val:                 val,
		PrimaryKeyAutoType:  primaryKeyAutoType,
	}
}
