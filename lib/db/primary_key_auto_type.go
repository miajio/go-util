package db

type PrimaryKeyAutoType int64

const (
	None       PrimaryKeyAutoType = iota // 不自动生成
	AutoNumber                           // 自增长
	AutoUUID                             // 随机生成UUID 随机生成的UUID将会是32位并去除了'-'符号并以大写形式出现
	AutoTime                             // 取当前时间戳->不建议使用该类型 如果要使用该类型 则需要将数据库主键改为 bigint
	AutoFunc                             // 由调用者提供自动生成方法进行方法调用后获取对于key并写入
)
