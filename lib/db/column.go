package db

import "reflect"

// Column 列字段结构体
type Column struct {
	IsPrimaryKey bool         // 是否为主键
	Name         string       // 字段名称
	Value        interface{}  // 字段值
	TypeOf       reflect.Type // 字段类型
}
