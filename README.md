# go-util

### 介绍
golang 开发工具库

本开源方案已上[pkg.go.dev](https://pkg.go.dev/gitee.com/miajio/go-util)
可通过go mod 自动引入

"gitee.com/miajio/go-util"

[![Fork me on Gitee](https://gitee.com/miajio/go-util/widgets/widget_3.svg)](https://gitee.com/miajio/go-util)



### lib.db库

封装基础的sql语句自动生成工具

sql语句内核目前版本主要可用的数据库为主流的sql形数据库

例如: oracle mysql mariadb mssql等

生成的sql语句均以预编译方式处理,外置执行sql方案建议使用sqlx(github.com/jmoiron/sqlx)

#### 自动生成Insert SQL语句
```
import "gitee.com/miajio/go-util/lib/db"

auto := db.AutoSQL.Create("id", "db", db.None, DBModel{})

sql, params , err := auto.Insert("table_name", DBModel{
    param key value...
})

```

#### 自动生成依据主键查询的Select SQL语句
```
import "gitee.com/miajio/go-util/lib/db"

auto := db.AutoSQL.Create("id", "db", db.None, DBModel{})

sql, params, err := auto.SelectByPrimaryKey("table_name", DBModel{
    primary key value ...
})
```

#### 自动生成依据主键删除的Delete SQL语句
```
import "gitee.com/miajio/go-util/lib/db"

auto := db.AutoSQL.Create("id", "db", db.None, DBModel{})

sql, params, err := auto.DeleteByPrimaryKey("table_name", DBModel{
    primary key value ...
})
```

#### 自动生成依据主键修改的Update SQL语句
```
import "gitee.com/miajio/go-util/lib/db"

auto := db.AutoSQL.Create("id", "db", db.None, DBModel{})

sql, params, err := auto.UpdateByPrimaryKey("table_name", DBModel{
    param key value...
})
```

#### lib.db库基础示例测试代码均使用*_*_test.go方式写在代码之中

### lib.read库

read库主要作用于读取结构体标签及判定结构体数据状态的处理库

后续可能会依据此库增加其他数据的读取方案

### lib.log库

此库基于uber zaplog库进行极简化封装

让用户更快捷的配置及获取log操作对象
```
l := &log.LoggerParam{
    Path:       "./",
    MaxSize:    512,
    MaxBackups: 16,
    MaxAge:     7,
    Compress:   true,
}
c := l.New(map[string]func(level zapcore.Level) bool{
    "info.log": func(level zapcore.Level) bool {
        return level < zapcore.WarnLevel && level >= zap.InfoLevel
    },
})
c.Info("success")
```

### lib.data库

基于基础数据操作的简单封装,例如filter过滤及distinct去重

### lib.sqlplus库

更全面的sql语句自动化生成封装方案,让开发者更快捷的操纵sql语句

此库是基于db库的更精细化逻辑封装,后续可能将替换lib.db库


---

[![miajio/go-util](https://gitee.com/miajio/go-util/widgets/widget_card.svg?colors=ff0505,2e2f29,272822,484a45,4dff00,747571)](https://gitee.com/miajio/go-util)